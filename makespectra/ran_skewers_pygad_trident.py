'''
Script to generate random skewers through simulations using pygad and trident (yt).
Modified by: Daniele Sorini, 27 Mar 2019
Original version: Kate Storey-Fisher, June 20, 2017
'''

import numpy as np
import time
import pylab as plt
import sys

import pygad as pg
import yt
from yt.utilities.cosmology import Cosmology
import trident
from trident import LightRay
import caesar
import h5py
from astropy.cosmology import FlatLambdaCDM
from pygadgetreader import *
from astropy.io import ascii

Nskew = 10 # Nr. of skewers
Nbins=500 # Nr. of bins along spectra
snr = 100.0
periodic_vel = True

c = pg.physics.c.in_units_of('km/s')
kpc2cm = 3.085678e21

MODEL = sys.argv[1]
SNAP = int(sys.argv[2])
WIND = sys.argv[3]
zout = sys.argv[4]
print('z=%s'%(zout))
snap_file = '/home/rad/data/%s/%s/snap_%s_%03d.hdf5' % (MODEL,WIND,MODEL,SNAP)
infile = '/home/rad/data/%s/%s/Groups/%s_%03d.hdf5' % (MODEL,WIND,MODEL,SNAP)
ofpath='/disk01/sorini/outputs/skewers/tests/'

TINIT = time.time()

def t_elapsed(): return np.round(time.time()-TINIT,2)

def generate_pygad_spectrum(s, line_name, los, spec_name, lambda_rest, i, Nbins):
    print("LOS=",los)
    print("Generating pygad spectrum {}...".format(spec_name))

    H = s.cosmology.H(s.redshift).in_units_of('(km/s)/Mpc', subs=s)
    box_width = s.boxsize.in_units_of('Mpc', subs=s)
    vbox = H * box_width
    if periodic_vel: v_limits = [-0.5*vbox, 1.5*vbox]
    else: v_limits = [0, vbox]
        
    taus, col_densities, phys_densities, temps, vz, v_edges, restr_column = pg.analysis.absorption_spectra.mock_absorption_spectrum_of(s, los, line_name, v_limits, Nbins=Nbins)  ## DS: No Standard version
    print s.redshift
    taus = taus/(1.+s.redshift)/(1.+s.redshift) # DS: accouints for the fact that Pygad computes comoving col. dens.
    col_densities = col_densities/(1.+s.redshift)/(1.+s.redshift) # DS see above
    velocities = 0.5 * (v_edges[1:] + v_edges[:-1])
    print("vmin=%s vmax=%s"%(velocities[0],velocities[-1]))
    wavelengths = lambda_rest * (s.redshift + 1) * (1 + velocities / c)
    sigma_noise = 1.0/snr
    noise = np.random.normal(0.0, sigma_noise, len(wavelengths))
    noise_vector = [sigma_noise] * len(noise)
    # periodically wrap velocities into the range [-vbox,0] -- should only be used when generating *random* LOS

    col_dens_old = np.copy(col_densities) # DS: Needed to periodically wrap quantities other than col_dens and tau in the correct way.
                                          # Whereas col_dens and tau are additive, all other quantities need to be properly weighted
                                          # by the column density of the atom/ion that is considered (in my case, HI).
                                          # For this reason, I make here a copy of the column density skewer given by Pygad, which is
                                          # not periodic at this stage.
    if periodic_vel:
        npix_periodic = int( vbox / (max(velocities)-min(velocities)) * len(wavelengths) ) 
        print 'periodically wrapping optical depths with %d pixels'%npix_periodic
        for j in range(0,len(wavelengths)):
            if velocities[j] < 0:
                taus[j+npix_periodic] += taus[j]
                col_densities[j+npix_periodic] += col_densities[j]
                # DS: col_dens and tau are now periodically wrapped (they are additive)
                # DS: In the next three lines, all other quantities are periodically wrapped. To do this, they need to be multiplied by the old, non-periodic col_dens (which was 
                # saved earlier), and then they have to be added to the relevant quantity multiplied by the new column density (periodic), and normalised by such col. dens.
                phys_densities[j+npix_periodic] = (phys_densities[j+npix_periodic]*col_dens_old[j+npix_periodic]+phys_densities[j]*col_densities[j])/col_densities[j+npix_periodic]
                temps[j+npix_periodic] = (temps[j+npix_periodic]*col_dens_old[j+npix_periodic]+temps[j]*col_densities[j])/col_densities[j+npix_periodic]
                vz[j+npix_periodic] = (vz[j+npix_periodic]*col_dens_old[j+npix_periodic]+vz[j]*col_densities[j])/col_densities[j+npix_periodic]
            if velocities[j] > vbox:
                taus[j-npix_periodic] += taus[j]
                col_densities[j-npix_periodic] += col_densities[j]
                phys_densities[j-npix_periodic] = (phys_densities[j-npix_periodic]*col_dens_old[j-npix_periodic]+phys_densities[j]*col_densities[j])/col_densities[j-npix_periodic]
                temps[j-npix_periodic] = (temps[j-npix_periodic]*col_dens_old[j-npix_periodic]+temps[j]*col_densities[j])/col_densities[j-npix_periodic]
                vz[j-npix_periodic] = (vz[j-npix_periodic]*col_dens_old[j-npix_periodic]+vz[j]*col_densities[j])/col_densities[j-npix_periodic]
    flux = np.exp(-np.array(taus))
    fluxes = np.exp(-np.array(taus)) + noise

    if periodic_vel:
        mask=np.logical_and(velocities<=vbox,velocities>=0)
        col_densities=col_densities[mask]
        phys_densities=phys_densities[mask]
        temps=temps[mask]
        vz=vz[mask]
        taus=taus[mask]
        fluxes=fluxes[mask]
        noise=noise[mask]
        velocities=velocities[mask]

    # Save spectrum to hdf5 format
    with h5py.File(spec_name, 'w') as hf:
        hf.create_dataset('velocity', data=np.array(velocities))
        hf.create_dataset('flux', data=np.array(fluxes))
        hf.create_dataset('wavelength', data=np.array(wavelengths))
        hf.create_dataset('tau', data=np.array(taus))
        hf.create_dataset('noise', data=np.array(noise)) # Modified by DS: used to be noise_vector
        hf.create_dataset('density_col', data=np.array(col_densities))
        hf.create_dataset('density_phys', data=np.array(phys_densities))
        hf.create_dataset('temp', data=np.array(temps))
        hf.create_dataset('los_vpec', data=np.array(vz))
    print('Pygad spectrum done [t=%g s]'%(t_elapsed()))


def generate_trident_spectrum(ds, line_name, los, spec_name, lambda_rest, i):
    print('LOS = ',los)

    print("Generating trident spectrum...")
    ray_start = [los[0], los[1], ds.domain_left_edge[2]]
    ray_end = [los[0], los[1], ds.domain_right_edge[2]]
    line_list = [line_name]
    ray = trident.make_simple_ray(ds,
                                  start_position=ray_start,
                                  end_position=ray_end,
                                  data_filename="ray.h5",
                                  lines=line_list,
                                  ftype='PartType0',
                                  fields=["density"])

    ar = ray.all_data()
    print 'ray=',ray,ar

    LR = trident.LightRay(ds)
    light_ray = LR.make_light_ray(start_position=ray_start,
                                  end_position=ray_end,
                                  data_filename="light_ray.h5",
                                  #lines=line_list,
                                  #ftype='PartType0',
                                  fields=["density", "temperature", "H_number_density"],
                                  periodic=True)

    light_ar = light_ray.all_data()
    print 'light ray=',light_ray,light_ar

    # Set up trident spectrum generator and generate specific spectrum using that ray
    co = Cosmology()
    print co.hubble_parameter(ds.current_redshift).in_units("km/s/kpc"),ds.hubble_constant
    vbox = ds.domain_right_edge[2] * co.hubble_parameter(ds.current_redshift).in_units("km/s/kpc") / ds.hubble_constant / (1.+ds.current_redshift)
    if periodic_vel:
        lambda_min = lambda_rest * (1 + ds.current_redshift) * (1. - 1.5*vbox.value / c)
        lambda_max = lambda_rest * (1 + ds.current_redshift) * (1. + 0.5*vbox.value / c)
    else:
        lambda_min = lambda_rest * (1 + ds.current_redshift) * (1. - 1.0*vbox.value / c)
        lambda_max = lambda_rest * (1 + ds.current_redshift) * (1. + 0.0*vbox.value / c)
    print 'vbox=',vbox,lambda_rest,lambda_min,lambda_max

    h5in = h5py.File('ray.h5', 'r')
    print(h5in)
    phys_densities = np.array(h5in['/grid/density'])
    if periodic_vel:
        Nbins = 2*len(phys_densities)
    else:
        Nbins = len(phys_densities)
    print 'Nbins = ', Nbins

    sg = trident.SpectrumGenerator(lambda_min=lambda_min, lambda_max=lambda_max, n_lambda=Nbins)

    #sg = trident.SpectrumGenerator('COS-G130M')
    sg.make_spectrum(ray, lines=line_list)
    
    # Get fields and convert wavelengths to velocities. Note that the velocities are negative compared to pygad,
    # so an adjustment must be made to the velocities/wavelengths to directly compare the two.
    wavelengths = np.array(sg.lambda_field)
    
    taus = np.array(sg.tau_field)
    sigma_noise = 1.0/snr
    noise = np.random.normal(0.0, sigma_noise, len(wavelengths))
    noise_vector = [sigma_noise] * len(noise)
    fluxes = np.array(sg.flux_field) + noise
    velocities = c * ((wavelengths / lambda_rest) / (1.0 + ds.current_redshift) - 1.0)
    print("The length of Trident arrays is %s cells"%(len(wavelengths)))
    print 'Trident generated spectrum v=',min(velocities),max(velocities)

    # periodically wrap velocities into the range [-vbox,0] -- should only be used when generating *random* LOS                                                                                                                            
    fluxes_orig = fluxes
    # DS: Only taus need to be made periodic here, as Trident should already take care  of the periodicity of the other quantities
    # (see Trident documentation, make_light_ray
    if periodic_vel:
        npix_periodic = int( vbox / (max(velocities)-min(velocities)) * len(wavelengths) )
        print 'periodically wrapping optical depths with %d pixels'%npix_periodic
        for j in range(0,len(wavelengths)):
            if velocities[j] < -vbox:
                taus[j+npix_periodic] += taus[j]
            if velocities[j] > 0:
                taus[j-npix_periodic] += taus[j]
            if velocities[j] > vbox:
                taus[j-npix_periodic] -= taus[j]
            fluxes = np.exp(-taus) + noise

    plt.plot(velocities,fluxes,':',c='g',label='Trident')
    plt.plot(velocities,noise_vector,'--',c='c')

    print 'vbox=', vbox
    print 'vmin =', velocities[0], 'vmax=', velocities[-1], 'len=', len(velocities)

    if periodic_vel:
        mask=np.logical_and(velocities>=-vbox.value,velocities<=0)
        taus=taus[mask]
        fluxes=fluxes[mask]
        noise=noise[mask]
        velocities=-velocities[mask] #DS The minus at the beginning is needed for comparison with pygad                                                                                                                                     
    else:
        velocities=-velocities
    print 'vmin =', velocities[0], 'vmax=', velocities[-1], 'len=', len(velocities)


    # Read all other fields from ray.h5 file                                                                                                                                                                                                
    print 'RAY'
    print(h5in)
    nHI_densities = np.array(h5in['/grid/H_number_density'])
    a = 1./(1+sim.simulation.redshift)
    gridcell=kpc2cm*a*Lbox/(h*len(taus)) # cm                                                                                                                                                                                              
    col_densities=nHI_densities*gridcell # g cm^-2                                                                                                                                                                                        
    temps= np.array(h5in['/grid/temperature'])
    vz= -1.e-5*np.array(h5in['/grid/velocity_los']) # km/s # DS The minusis needed for a comparison with Pygad                                                                                                                              
    print 'LOS Velocity field included; shape ', np.shape(vz)

    #Save spectrum to hdf5 format                                                                                                                                                                                                           
    #outfile = ofpath+'spectrum_trident_%s%sz%s_%s.h5'%(MODEL,WIND,zout,i)
    #with h5py.File(outfile, 'w') as hf:                                                                                                                                                                                                    
    with h5py.File(spec_name, 'w') as hf:
        hf.create_dataset('velocity', data=np.array(np.flip(velocities)))
        hf.create_dataset('flux', data=np.array(np.flip(fluxes)))
        hf.create_dataset('wavelength', data=np.array(wavelengths))
        hf.create_dataset('tau', data=np.array(np.flip(taus)))
        hf.create_dataset('noise', data=np.array(np.flip(noise)))
        hf.create_dataset('density_col', data=np.array(col_densities))
        hf.create_dataset('density_phys', data=np.array(phys_densities))
        hf.create_dataset('nHI_phys', data=np.array(nHI_densities))
        hf.create_dataset('temp', data=np.array(temps))
        hf.create_dataset('los_vpec', data=np.array(vz))
    print('Trident spectrum done [t=%g s]'%(t_elapsed()))


    print 'LIGHT RAY'
    h5in = h5py.File('light_ray.h5', 'r')
    print(h5in)
    phys_densities = np.array(h5in['/grid/density'])
    nHI_densities = np.array(h5in['/grid/H_number_density'])
    a = 1./(1+sim.simulation.redshift)
    gridcell=kpc2cm * a* Lbox / h / len(taus) # cm                                                                                                                                                                                         
    col_densities=nHI_densities*gridcell # g cm^-2                                                                                                                                                                                        
    temps= np.array(h5in['/grid/temperature'])
    vz= 1.e-5*np.array(h5in['/grid/velocity_los']) # km/s                                                                                                                                                                                  

    #Save spectrum to hdf5 format
    outfile = ofpath+'lightray_trident_%s%sz%s_%s.h5'%(MODEL,WIND,zout,i)
    #with h5py.File(outfile, 'w') as hf:                                                                                                                                                                                                    
    with h5py.File(outfile, 'w') as hf:
        hf.create_dataset('velocity', data=np.array(np.flip(velocities)))
        hf.create_dataset('flux', data=np.array(np.flip(fluxes)))
        hf.create_dataset('wavelength', data=np.array(wavelengths))
        hf.create_dataset('tau', data=np.array(np.flip(taus)))
        hf.create_dataset('noise', data=np.array(np.flip(noise)))
        hf.create_dataset('density_col', data=np.array(col_densities))
        hf.create_dataset('density_phys', data=np.array(phys_densities))
        hf.create_dataset('nHI_phys', data=np.array(nHI_densities))
        hf.create_dataset('temp', data=np.array(temps))
        hf.create_dataset('los_vpec', data=np.array(vz))
    print('Trident spectrum done [t=%g s]'%(t_elapsed()))


### Select snapshot, line and LOS
s = pg.Snap(snap_file)
ds = yt.load(snap_file)
sim = caesar.load(infile,LoadHalo=False)

Lbox = readheader(snap_file, 'boxsize') # kpc/h

cosmo = FlatLambdaCDM(H0=100*sim.simulation.hubble_constant, Om0=sim.simulation.omega_matter, Ob0=sim.simulation.omega_baryon,Tcmb0=2.73)
hubble = cosmo.H(sim.simulation.redshift)
h = sim.simulation.hubble_constant
print 'h = ', h
print 'z,H=',sim.simulation.redshift,hubble

line_tri = 'H I 1216'
line_pg = 'H1215'
lambda_rest = float(pg.analysis.absorption_spectra.lines[line_pg]['l'].split()[0])

# Generate Random Lines of Sight
np.random.seed(10)

for i in np.arange(Nskew):
    xlos = np.random.uniform(0., Lbox)
    ylos = np.random.uniform(0., Lbox)
    los = [xlos, ylos]
    print 'LOS=', los
    generate_pygad_spectrum(s, line_pg, los, ofpath+'spectrum_pygad_%s%sz%s_%s.h5'%(MODEL,WIND,zout,i), lambda_rest, i, Nbins=Nbins)
    #generate_trident_spectrum(ds, line_tri, los, ofpath+'spectrum_trident_%s%sz%s_%s.h5'%(MODEL,WIND,zout,i), lambda_rest, i)    
