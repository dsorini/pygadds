"""
Fit mock absorption spectra with Voigt profiles using the VAMP Bayesian VP fitter.

Romeel Dave, April 2019

History: VAMP was initially assembled by Chris Lovell in Fall 2016, with help
from Kate Storey-Fisher, to fit Gaussians to a spectrum via MCMC.  In Spring 2018, 
Sarah Appleby made some improvements to compute physical quantities from the
best-fit Gaussians.  In Fall 2018/Spring 2019, Jacob Christiansen added various other
useful features, extensively debugged the code, and made it into a usable package.
In Spring 2019, Romeel Dave tweaked the algorithm to make it more workable.
and inserted the VAMP package into pygad.

"""

__all__ = ['fit_spectrum', 'fit_region', 'find_regions', 'plot_spectrum', 'output_spectrum' ]

from ..units import Unit, UnitArr, UnitQty, UnitScalar
from ..physics import kB, m_H, c, q_e, m_e, epsilon0

import matplotlib
#matplotlib.use('agg')
import datetime

import numpy as np
import pymc as mc
import matplotlib.pyplot as plt

# Voigt modules
from astropy.modeling.models import Voigt1D
# peak finding modules
from scipy.signal import find_peaks_cwt
from scipy.signal import savgol_filter

# gaussian smoothing modules
from scipy.ndimage.filters import gaussian_filter
from scipy.signal import argrelextrema

from copy import copy
import os
import h5py


#TODO: change the way that "Models are instantiated" (need to understand that first)
import warnings
warnings.filterwarnings("ignore", message="Instantiating a Model object directly is deprecated. We recommend passing variables directly to the Model subclass.")

from .. import environment
from pygad import physics
from absorption_spectra import line_quantity


#####################################################
# Class for profile fitting
#####################################################

class VPfit():

    def __init__(self, noise=None):
        """
        Initialise noise variable
        """
        self.std_deviation = 1./(mc.Uniform("sd", 0, 1) if noise is None else noise)**2
        self.verbose = False


    @staticmethod
    def GaussFunction(x, amplitude, centroid, sigma):
        """
        Gaussian.

        Args:
            x (numpy array): wavelength array
            amplitude (float)
            centroid (float): must be between the limits of wavelength_array
            sigma (float)
        """
        return amplitude * np.exp(-0.5 * ((x - centroid) / sigma) ** 2)


    @staticmethod
    def VoigtFunction(x, centroid, amplitude, L_fwhm, G_fwhm):
        """
        Return the Voigt line shape at x with Lorentzian component HWHM gamma
        and Gaussian component HWHM alpha.

        Source: http://scipython.com/book/chapter-8-scipy/examples/the-voigt-profile/

        Args:
            x (numpy array): wavelength array
            centroid (float): center of profile
            alpha (float): Gaussian HWHM
            gamma (float): Lorentzian HWHM
        """

        #sigma = alpha / np.sqrt(2 * np.log(2))
        #return np.real(wofz((x - centroid + 1j*gamma)/sigma/np.sqrt(2))) / sigma / np.sqrt(2*np.pi)

        v = Voigt1D(x_0=centroid, amplitude_L=amplitude, fwhm_L=L_fwhm, fwhm_G=G_fwhm)
        return v(x)


    @staticmethod
    def GaussianWidth(G_fwhm):
        """
        Find the std deviation of the Gaussian associated with a Voigt profile, from the
        Full Width Half Maximum (FWHM)

        Args:
            G_fwhm (float): Full Width Half Maximum (FWHM)
        """
        return G_fwhm / (2.* np.sqrt(2.*np.log(2.)))
    

    def trunc_normal(self, name, mu, tau, a, b):
        """
        Take a sample from a truncated Gaussian distribution with PyMC Normal.
        Args:
            name (string): name of the component
            mu (float): mean of the distribution
            tau (float): precision of the distribution, which corresponds to 1/sigma**2 (tau > 0).
            a (float): lower bound of the distribution
            b (float): upper bound of the distribution
        """
        for i in range(1000):
            x = pymc.Normal(name, mu, tau)
            if (a < x.value) and (b > x.value):
                break
        if (a > x.value) or (b < x.value):
            raise ValueError('Computational time exceeded.')    
        return x

    @staticmethod
    def Chisquared(observed, expected, noise):
        """
        Calculate the chi-squared goodness of fit

        Args:
            observed (array)
            expected (array): same shape as observed
        """
        return sum( ((observed - expected) /  noise)**2 )


    @staticmethod
    def ReducedChisquared(observed, expected, noise, freedom):
        """
        Calculate the reduced chih-squared goodness of fit

        Args:
            observed (array)
            expected (array): same shape as observed
            freedom (int): degrees of freedom
        """
        return VPfit.Chisquared(observed, expected, noise) / freedom


    def plot(self, wavelength_array, flux_array, clouds=None, n=1, onesigmaerror = 0.02, 
            start_pix=None, end_pix=None, filename=None):
        """
        Plot the fitted absorption profile

        Args:
            wavelength_array (numpy array):
            flux_array (numpy array): original flux array, same length as wavelength_array
            filename (string): for saving the plot
	    clouds (pandas dataframe): dataframe containing details on each absorption feature
            n (int): number of *fitted* absorption profiles
            onesigmaerror (float): noise on profile plot
        """

        if not start_pix:
            start_pix = 0
        if not end_pix:
            end_pix = len(wavelength_array)

        f, (ax1, ax2, ax3) = plt.subplots(3, sharex=True, sharey=False, figsize=(10,10))

        ax1.plot(wavelength_array, (flux_array - self.total.value) / onesigmaerror)
        ax1.hlines(1, wavelength_array[0], wavelength_array[-1], color='red', linestyles='-')
        ax1.hlines(-1, wavelength_array[0], wavelength_array[-1], color='red', linestyles='-')
        ax1.hlines(3, wavelength_array[0], wavelength_array[-1], color='red', linestyles='--')
        ax1.hlines(-3, wavelength_array[0], wavelength_array[-1], color='red', linestyles='--')

        ax2.plot(wavelength_array, flux_array, color='black', linewidth=1.0)

        if clouds is not None:
            for c in range(len(clouds)):
                if c==0:
                    ax2.plot(wavelength_array, Tau2flux(clouds.ix[c]['tau'][start_pix:end_pix]),
                             color="red", label="Actual", lw=1.5)
                else:
                    ax2.plot(wavelength_array, Tau2flux(clouds.ix[c]['tau'][start_pix:end_pix]),
                             color="red", lw=1.5)


        for c in range(n):
            if c==0:
                ax2.plot(wavelength_array, Tau2flux(self.estimated_profiles[c].value),
                         color="green", label="Fit")
            else:
                ax2.plot(wavelength_array, Tau2flux(self.estimated_profiles[c].value),
                         color="green")


        ax2.legend()

        ax3.plot(wavelength_array, flux_array, label="Measured")
        ax3.plot(wavelength_array, self.total.value, color='green', label="Fit", linewidth=2.0)
        ax3.legend()

        f.subplots_adjust(hspace=0)

        if hasattr(self, 'fit_time'): ax1.set_title("Fit time: " + self.fit_time)
        ax1.set_ylabel("Residuals")
        ax2.set_ylabel("Normalised Flux")
        ax3.set_ylabel("Normalised Flux")
        ax3.set_xlabel("$ \lambda (\AA)$")

        if filename:
            plt.savefig(filename)
        else:
            plt.show()


    def find_local_minima(self, f_array, window=101):
        """
        Find the local minima of an absorption profile.

        Args:
            f_array: flux array
            window: smoothing window, pixels
        Returns:
            indices of local minima in flux_array
        """

        # smooth flux profile
        smoothed_flux = savgol_filter(f_array, window, 1)

        return find_peaks_cwt(smoothed_flux * -1, np.array([window/3]))


    def initialise_components(self, frequency_array, n, sigma_max):
        """
        Initialise each fitted component of the model in optical depth space. Each component 
        consists of three variables, height, centroid and sigma. These variables are encapsulated 
        in a deterministic profile variable. The variables are stored in a dictionary, `estimated_variables`, 
        and the profiles in a list, `estimated_profiles`.

        Args:
            frequency_array (numpy array)
            n (int): number of components
            sigma_max (float): maximum permitted range of fitted sigma values
        """

        self.estimated_variables = {}
        self.estimated_profiles = []

        for component in range(n):

            self.estimated_variables[component] = {}

            @mc.stochastic(name='xexp_%d' % component)
            def xexp(value=0.5):
                if value < 0:
                    return -np.inf
                else:
                    return np.log(value * np.exp(-value))

            self.estimated_variables[component]['amplitude'] = xexp
            #self.estimated_variables[component]['height'] = mc.Uniform("est_height_%d" % component, 0, 5)

            self.estimated_variables[component]['centroid'] = mc.Uniform("est_centroid_%d" % component,
                                                                         frequency_array[0], frequency_array[-1])

            self.estimated_variables[component]['sigma'] = mc.Uniform("est_sigma_%d" % component, 0, sigma_max)

            @mc.deterministic(name='component_%d' % component, trace = True)
            def profile(x=frequency_array,
                        centroid=self.estimated_variables[component]['centroid'],
                        sigma=self.estimated_variables[component]['sigma'],
                        height=self.estimated_variables[component]['amplitude']):

                return self.GaussFunction(x, height, centroid, sigma )

            self.estimated_profiles.append(profile)


    def initialise_voigt_profiles(self, frequency_array, n, sigma_max, local_minima=[]):
        """
        Args:
            frequency_array (numpy array)
            n (int): number of components
            sigma_max (float): maximum permitted range of fitted sigma values
        """

        #if n < len(local_minima):
        #    raise ValueError("Less profiles than number of minima.")

        self.estimated_variables = {}
        self.estimated_profiles = []

        for component in range(n):

            self.estimated_variables[component] = {}

            @mc.stochastic(name='xexp_%d' % component)
            def xexp(value=0.5):
                if value < 0:
                    return -np.inf
                else:
                    return np.log(value * np.exp(-value))

            self.estimated_variables[component]['amplitude'] = xexp

            self.estimated_variables[component]['centroid'] = mc.Uniform("est_centroid_%d" % component,
                                                                            frequency_array[0], frequency_array[-1])


            self.estimated_variables[component]['L_fwhm'] = mc.Uniform("est_L_%d" % component, 0, sigma_max)
            self.estimated_variables[component]['G_fwhm'] = mc.Uniform("est_G_%d" % component, 0, sigma_max)

            @mc.deterministic(name='component_%d' % component, trace = True)
            def profile(x=frequency_array,
                        centroid=self.estimated_variables[component]['centroid'],
                        amplitude=self.estimated_variables[component]['amplitude'],
                        L=self.estimated_variables[component]['L_fwhm'],
                        G=self.estimated_variables[component]['G_fwhm']):
                return self.VoigtFunction(x, centroid, amplitude, L, G)

            self.estimated_profiles.append(profile)


    def initialise_model(self, frequency, flux, n, local_minima=[], voigt=False):
        """
        Initialise deterministic model of all absorption features, in normalised flux.

        Args:
            frequency (numpy array)
            flux (numpy array): flux values at each wavelength
            n (int): number of absorption profiles to fit
        """
        
        self.sigma_max = (frequency[-1] - frequency[0]) / 2.

        # always reinitialise profiles, otherwise starts sampling from previously calculated parameter values.
        if(voigt):
            if self.verbose:
                print "Initialising Voigt profile components."
            self.fwhm_max = self.sigma_max * 2*np.sqrt(2*np.log(2.))
            self.initialise_voigt_profiles(frequency, n, self.fwhm_max, local_minima=local_minima)
        else:
            if self.verbose:
                print "Initialising Gaussian profile components."
            self.initialise_components(frequency, n, self.sigma_max)

        # deterministic variable for the full profile, given in terms of normalised flux
        @mc.deterministic(name='profile', trace=False)
        def total(profile_sum=self.estimated_profiles):
            return Tau2flux(sum(profile_sum))

        self.total = total

        # represent full profile as a normal distribution
        self.profile = mc.Normal("obs", self.total, self.std_deviation, value=flux, observed=True)

        # create model with parameters of all profiles to be fitted
        # TODO: fix the deprecation error (this is the only case where a "Model" is instantiated):
        # /home/jacobc/anaconda2/envs/vamp27pymc237/lib/python2.7/site-packages/pymc/MCMC.py:81: 
        # UserWarning: Instantiating a Model object directly is deprecated. We recommend passing variables directly to the Model subclass.  warnings.warn(message)

        self.model = mc.Model([self.estimated_variables[x][y] for x in self.estimated_variables \
                            for y in self.estimated_variables[x]])# + [std_deviation])


    def map_estimate(self, iterations=2000):
        """
        Compute the Maximum A Posteriori estimates for the initialised model
        """

        self.map = mc.MAP(self.model)
        self.map.fit(iterlim=iterations, tol=1e-3)

        
    def mcmc_fit(self, iterations=15000, burnin=100, thinning=15, step_method=mc.AdaptiveMetropolis):
        """
        MCMC fit of `n` absorption profiles to a given spectrum

        Args:
            iterations (int): How long to sample the MCMC for
            burnin (int): How many iterations to ignore from the start of the chain
            thinning (int): How many iterations to ignore each tally
            step_method (PyMC steo method object)
        """

        try:
            getattr(self, 'map')
        except AttributeError:
            print "\nWARNING: MAP estimate not provided. \nIt is recommended to compute this \
            in advance of running the MCMC so as to start the sampling with good initial values."

        # create MCMC object
        self.mcmc = mc.MCMC(self.model)

        # change step method
        if step_method != mc.AdaptiveMetropolis:
            print "Changing step method for each parameter."
            for index, item in self.estimated_variables.items():
                self.mcmc.use_step_method(step_method, item['sigma'])
                self.mcmc.use_step_method(step_method, item['centroid'])
                self.mcmc.use_step_method(step_method, item['height'])
        else:
            print "Using Adaptive Metropolis step method for each parameter."

        # fit the model
        starttime=datetime.datetime.now()
        self.mcmc.sample(iter=iterations, burn=burnin, thin=thinning)
        self.fit_time = str(datetime.datetime.now() - starttime)
        print "\nTook:", self.fit_time, " to finish."


    def find_bic(self, frequency_array, flux_array, n, noise_array, voigt=False, 
                iterations=3000, thin=15, burn=300, thorough=False):
        """
        Initialise the Voigt model and run the MCMC fitting for a particular number of 
        regions and return the Bayesian Information Criterion. Used to identify the 
        most appropriate number of profles in the region.
        
        Args:
            frequency_array (numpy array)
            flux_array (numpy array)
            n (int): number of minima to fit in the region
            noise_array (numpy array)
            voigt (Boolean): switch to fit as Voigt profile or Gaussian
            iterations, thin, burn (ints): MCMC parameters
        """
        
        self.bic_array = []
        self.red_chi_array = []
        freedom = len(frequency_array)-3*n
        for i in range(1):
            self.initialise_model(frequency_array, flux_array, n, voigt=voigt)
            self.map = mc.MAP(self.model)
            self.mcmc = mc.MCMC(self.model)
            if thorough:
                self.map.fit(iterlim=iterations, tol=1e-3)
                self.mcmc.sample(iter=iterations, burn=burn, thin=thin, progress_bar=False)
                self.map.fit(iterlim=iterations, tol=1e-3)
            self.mcmc.sample(iter=iterations, burn=burn, thin=thin, progress_bar=False)
            self.map.fit(iterlim=iterations, tol=1e-3)
            self.bic_array.append(self.map.BIC)
            self.red_chi_array.append(self.ReducedChisquared(flux_array, self.total.value, noise_array, freedom))
        return


    def chain_covariance(self, n, voigt=False):
        """
        Find the covariance matrix of the MCMC chain variables
        Args:
            n (int): the number of lines in the region
            voigt (boolean): switch to use Voigt parameters
        Returns:
            cov (numpy array): the covariance matrix
            cov =   [ var(amplitude),           cov(amplitude, sigma),  cov(amplitude, centroid) ]
                    [ cov(sigma, amplitude),    var(sigma),             cov(sigma, centroid)     ]
                    [ cov(centroid, amplitude), cov(centroid, sigma),   var(centroid)            ]

        """
        cov = np.zeros((n, 3, 3))
        for i in range(n):
            amp_samples = self.mcmc.trace('xexp_'+str(i))[:]
            if not voigt:
                sigma_samples = self.mcmc.trace('est_sigma_'+str(i))[:]
            elif voigt:
                gfwhm_samples = self.mcmc.trace('est_G_'+str(i))[:]
                sigma_samples = self.GaussianWidth(gfwhm_samples)
            c_samples = self.mcmc.trace('est_centroid_'+str(i))[:]
        
            cov[i] = np.cov(np.array((amp_samples, sigma_samples, c_samples)))
        return cov

#####################################################
# Physical quantities computation routines
#####################################################

def Tau2flux(tau):
    """
    Convert optical depth to normalised flux profile.

    Args:
        tau (numpy array): array of optical depth values
    """
    return np.exp(-tau)

def EquivalentWidth(fluxes, waves):
    """
    Find the equivalent width of a line/region.

    Args:
        taus (numpy array): the optical depths.
        waves (numpy array): list of wavelength for region.
    Returns:
        Equivalent width in units of waves.
    """
    dEW = np.zeros(len(fluxes))
    for i in range(1,len(fluxes)-1):
        dEW[i] = (1. - fluxes[i]) * abs(waves[i+1]-waves[i-1]) * 0.5
    dEW[0] = (1. - fluxes[0]) * abs(waves[1]-waves[0])
    dEW[i-1] = (1. - fluxes[i-1]) * abs(waves[i-1]-waves[i-2])
    return np.sum(dEW)

def ErrorN(amplitude, sigma, std_a, std_s, cov_as, sigma_0):
    """
    Evaluate the standard deviation on the column density N from the standard deviations
    of the profile fit parameters
    Args:
        amplitude (numpy array): the amplitudes of the profile fit in frequency space.
        sigma (numpy array): the widths of the profile fit in frequency space.
        std_a (numpy array): the standard deviations of the amplitude
        std_s (numpy array): the standard deviations of the widths
        cov_as (numpy array): the covariance of amplitude and width
        sigma_0 (float): cross section for transition
    """
    prefactor = np.sqrt(2.*np.pi) / sigma_0 # where does this number come from?
    amp_part = sigma**2 * std_a **2
    sig_part = amplitude**2 * std_s**2
    #cov_part = 2*cov_as * amplitude * sigma
    #return prefactor * np.sqrt(amp_part + sig_part + cov_part)
    return prefactor * np.sqrt(amp_part + sig_part) #currently ignoring covariance


#####################################################
# Region detection routine
#####################################################

def find_regions(wavelengths, fluxes, noise, min_region_width=2, N_sigma=4.0, extend=False):
    """
    Finds detection regions above some detection threshold and minimum width.

    Args:
        wavelengths (numpy array)
        fluxes (numpy array): flux values at each wavelength    
        noise (numpy array): noise value at each wavelength 
        min_region_width (int): minimum width of a detection region (pixels)
        N_sigma (float): detection threshold (std deviations)
        extend (boolean): default is False. Option to extend detected regions untill tau
                        returns to continuum.

    Returns:
        regions_l (numpy array): contains subarrays with start and end wavelengths
        regions_i (numpy array): contains subarrays with start and end indices
    """

    num_pixels = len(wavelengths)
    pixels = range(num_pixels)
    min_pix = 1
    max_pix = num_pixels - 1

    flux_ews = [0.] * num_pixels
    noise_ews = [0.] * num_pixels
    det_ratio = [-float('inf')] * num_pixels

    # flux_ews has units of wavelength since flux is normalised. so we can use it for optical depth space
    for i in range(min_pix, max_pix):
        flux_dec = 1.0 - fluxes[i]
        if flux_dec < noise[i]:
            flux_dec = 0.0
        flux_ews[i] = 0.5 * abs(wavelengths[i - 1] - wavelengths[i + 1]) * flux_dec
        noise_ews[i] = 0.5 * abs(wavelengths[i - 1] - wavelengths[i + 1]) * noise[i]

    # dev: no need to set end values = 0. since loop does not set end values
    flux_ews[0] = 0.
    noise_ews[0] = 0.

    # Range of standard deviations for Gaussian convolution
    std_min = 2
    std_max = 11

    # Convolve varying-width Gaussians with equivalent width of flux and noise
    xarr = np.array([p - (num_pixels-1)/2.0 for p in range(num_pixels)])
    
    # this part can remain the same, since it uses EW in wavelength units, not flux
    for std in range(std_min, std_max):

        gaussian = VPfit.GaussFunction(xarr, 1.0, 0.0, std)

        flux_func = np.convolve(flux_ews, gaussian, 'same')
        noise_func = np.convolve(np.square(noise_ews), np.square(gaussian), 'same')

        # Select highest detection ratio of the Gaussians
        for i in range(min_pix, max_pix):
            noise_func[i] = 1.0 / np.sqrt(noise_func[i])
            if flux_func[i] * noise_func[i] > det_ratio[i]:
                det_ratio[i] = flux_func[i] * noise_func[i]

    # Select regions based on detection ratio at each point, combining nearby regions
    start = 0
    region_endpoints = []
    for i in range(num_pixels):
        if start == 0 and det_ratio[i] > N_sigma and fluxes[i] < 1.0:
            start = i
        elif start != 0 and (det_ratio[i] < N_sigma or fluxes[i] > 1.0):
            if (i - start) > min_region_width:
                end = i
                region_endpoints.append([start, end])
            start = 0

    # made extend a kwarg option
    # lines may not go down to 0 again before next line starts
    
    if extend:
        # Expand edges of region until flux goes above 1
        regions_expanded = []
        for reg in region_endpoints:
            start = reg[0]
            i = start
            while i > 0 and fluxes[i] < 1.0:
                i -= 1
            start_new = i
            end = reg[1]
            j = end
            while j < (len(fluxes)-1) and fluxes[j] < 1.0:
                j += 1
            end_new = j
            regions_expanded.append([start_new, end_new])

    else: regions_expanded = region_endpoints

    # Change to return the region indices
    # Combine overlapping regions, check for detection based on noise value
    # and extend each region again by a buffer
    regions_l = []
    regions_i = []
    buffer = 3
    for i in range(len(regions_expanded)):
        start = regions_expanded[i][0]
        end = regions_expanded[i][1]
        #TODO: this part seems to merge regions if they overlap - try printing this out to see if it can be modified to not merge regions?
        if i<(len(regions_expanded)-1) and end > regions_expanded[i+1][0]:
            end = regions_expanded[i+1][1]
        for j in range(start, end):
            flux_dec = 1.0 - fluxes[j]
            if flux_dec > abs(noise[j]) * N_sigma:
                if start >= buffer:
                    start -= buffer
                if end < len(wavelengths) - buffer:
                    end += buffer
                regions_l.append([wavelengths[start], wavelengths[end]])
                regions_i.append([start, end])
                break

    print('VAMP : Found {} detection regions.'.format(len(regions_l)))
    return np.array(regions_l), np.array(regions_i)


def estimate_n(flux_array):
    """
    Make initial guess for number of profiles in the region.  
    For now, just set to 1, and let the fitter decide to add more as needed.
        
    Args:
        flux_array (numpy array)
    """
        
    #n = argrelextrema(gaussian_filter(flux_array, 3), np.less)[0].shape[0]
    n = 1
    return n

def periodic_wrap(wave, flux, noise, wave_range):
    print 'Periodic wrap not implemented yet'
    return wave, flux, noise, 0.

def periodic_unwrap(wave, flux, noise, wave_range, l_offset):
    print 'Periodic unwrap not implemented yet'
    return wave, flux, noise


#####################################################
# Fitting routine
#####################################################

def fit_region(start, end, wavelength, flux, noise, voigt=False, BIC_factor=1.1,
                chi_limit=2.5, ntries=5, iterations=3000, thin=15, burn=300):
    """
    Fit a region with Gaussian/Voigt profiles using PyMC.
    Returns instance of VPfit() that contains all the fitted line information.

    Args:
        start, end: starting and ending pixel numbers for given region
        wavelength (numpy array)
        flux (numpy array)
        noise (numpy array)
        voigt (Boolean): switch to fit as Voigt profiles or Gaussians
        BIC_factor (float): factor by which BIC must be lowered in order to accept the new fit
        chi_limit (float): limit for satisfactory reduced chi squared
        ntries (int): max number of trials with a given number of lines before adding new line
        iterations, thin, burn (int): MCMC parameters
    """

    # initialise chisq loop
    chisq = np.inf
    tries = ntries
    frequency = np.flip(physics.c.in_units_of('Angstrom s**-1')/wavelength[start:end],0)  # we fit in frequency space
    n = estimate_n(flux)
    # Add lines until we get a reasonable chi-square
    while chisq > chi_limit:
        vpfit = VPfit()
        vpfit.find_bic(frequency, flux[start:end], n, noise[start:end], voigt=voigt, iterations=iterations, burn=burn)
        chisq = np.average(vpfit.red_chi_array)
        if chisq > chi_limit:   
            if tries > 0: tries -= 1  # keep trying, with same number of lines
            else: 
                n += 1  # ok, give up and add a line
                tries = ntries
    if environment.verbose >= environment.VERBOSE_NORMAL:
        print "VAMP : Found %d lines, giving chisq=%g."%(n,chisq),

    # Try adding lines so long as each new line lowers chisq and also lowers the BIC by more BIC_factor
    vpfit_old = copy(vpfit)
    if BIC_factor is not None:
        vpfit.find_bic(frequency, flux[start:end], n+1, noise[start:end], voigt=voigt, iterations=iterations, burn=burn)
        while np.average(vpfit.red_chi_array) < np.average(vpfit_old.red_chi_array) and np.average(vpfit.bic_array) < BIC_factor*np.average(vpfit_old.bic_array):
            n += 1
            if environment.verbose >= environment.VERBOSE_NORMAL:
                print " Adding line %d: BIC=%g -> %g, chisq=%g -> %g."%(n,np.average(vpfit_old.bic_array),np.average(vpfit.bic_array),np.average(vpfit_old.red_chi_array),np.average(vpfit.red_chi_array)),
            vpfit_old = copy(vpfit)  # accept the new line and try to add another
            vpfit.find_bic(frequency, flux[start:end], n+1, noise[start:end], voigt=voigt, iterations=iterations, burn=burn)

    # We have the final fit
    if environment.verbose >= environment.VERBOSE_NORMAL:
        print " Fitted %d lines: BIC=%g, chisq=%g"%(n,np.average(vpfit_old.bic_array),np.average(vpfit_old.red_chi_array))
    return vpfit_old,vpfit_old.red_chi_array[0]

#####################################################
# Main routine.  Call with something like:
# vamp.fit_spectrum(wavelength, flux, noise, 'H1215', voigt=args.voigt, chi_limit=3.0, outfile_spec='spec_vamp.hdf5')
#####################################################

def fit_spectrum(wavelength_array, flux_array, noise_array, line, voigt=False, chi_limit=2.5, mcmc_cov=False, get_mcmc_err=True, outfile_spec=None, periodic_wrap=None):
    """
    The main function. Takes an input spectrum, splits it into manageable regions, and fits 
    the individual regions using PyMC. Finally calculates the Doppler parameter b, the 
    column density N and the equivalent width and the centroids of the absorption lines.

    Args:
        wavelength_array (numpy array): in Angstroms
        noise_array (numpy array)
        flux_array (numpy array)
        line (float): the transition to fit
        voigt (boolean): switch to fit Voigt profile instead of Gaussian. 
        mcmc_cov (boolean): switch to do error propogation from the mcmc chain. Default: False
        get_mcmc_err (boolean): switch to find the standard deviation (of b and N) from the mcmc chain. Default: True
        outfile_spec (string): file to output spectrum information to.  Default: None
        periodic_wrap ([wavelength_start,wavelength_end]): for fitting, wrap spectrum so that highest fluxes are at the ends.  Default: None

    Returns:
        l (numpy array): Centroids of absorption lines, in Angstroms.
        b (numpy array): Doppler parameters, in km/s.
        N (numpy array): Column densities, in cm**-2
        ew (numpy array): Equivalent widths, in Angstroms.
    """

    environment.verbose = 2

    wavelength_array = UnitArr(wavelength_array, units='Angstrom')
    l0 = UnitScalar(line_quantity(line,'l'), 'Angstrom')  # rest wavelength
    ftrans = line_quantity(line,'f')  # oscillator strength
    sigma_0 = (ftrans* np.pi * q_e * q_e / (4*np.pi*epsilon0*m_e*c)).in_units_of('cm**2/s')

    # wrap spectrum periodically by appending to either side of spectrum
    if periodic_wrap is not None:
        wavelength_array, flux_array, noise_array, l_offset = periodic_wrap(wavelength_array, flux_array, noise_array, periodic_wrap)

    # fit continuum
    # fit_continuum(wavelength_array, flux_array, noise_array)

    # identify regions to fit in the spectrum
    regions, region_pixels = find_regions(wavelength_array, flux_array, noise_array, min_region_width=2)

    # dicts to store results
    line_params = {'region_number': np.array([]), 'l': np.array([]), 'dl': np.array([]), 'b': np.array([]), 'db': np.array([]), 'N': np.array([]), 'dN': np.array([]), 'EW': np.array([])}

    flux_model = {'flux': np.ones(len(flux_array)), 'wavelength': np.ones(len(flux_array)), 'noise': np.ones(len(flux_array)), 'model_flux': np.ones(len(flux_array)),
                'region_chisq': np.zeros(len(regions)), 'region_pixels': region_pixels, 'region_EW': np.zeros(len(regions)) }
                #'amplitude': np.array([]), 'sigmas': np.array([]), 'centers': np.array([]), 'region_numbers': np.array([]),
                #, 'std_a': np.array([]), 'std_s': np.array([]), 'std_c': np.array([]), 'cov_as': np.array([]) }

    vpfits = []
    chisq_fits = []
    for start, end in region_pixels:
        fit,chisq = fit_region(start, end, wavelength_array, flux_array, noise_array, voigt=voigt, chi_limit=chi_limit)
        vpfits.append(fit)
        chisq_fits.append(chisq)

    #flux_model['region_numbers'] = range(len(region_pixels))
    flux_model['flux'] = flux_array
    flux_model['wavelength'] = wavelength_array
    flux_model['noise'] = noise_array
    for j in range(len(region_pixels)):
        fit = vpfits[j]
        start = region_pixels[j][0]
        end = region_pixels[j][1]
        n = len(fit.estimated_profiles)
        flux_model['region_chisq'][j] = chisq_fits[j]
        flux_model['model_flux'][start:end] = np.flip(fit.total.value, 0)
        '''
        flux_model['tau'][start:end] = np.flip(fit.total.value, 0)
        flux_model['region_'+str(j)+'_wave'] = np.flip(wavelength_array[start:end], 0)
        flux_model['region_'+str(j)+'_flux'] = np.zeros((n, len(flux_array[start:end])))
        for k in range(n):
            flux_model['region_'+str(j)+'_flux'][k] = np.flip(Tau2flux(fit.estimated_profiles[k].value), 0)
        '''
        flux_model['region_EW'][j] = EquivalentWidth(np.flip(Tau2flux(fit.total.value)),np.flip(wavelength_array[start:end], 0))

        heights = np.array([fit.estimated_variables[i]['amplitude'].value for i in range(n)])
        centers = np.array([(physics.c.in_units_of('Angstrom s**-1')/fit.estimated_variables[i]['centroid'].value) for i in range(n)])
        if periodic_wrap is not None:
            wavelength_array, flux_array, noise_array = periodic_wrap(wavelength_array, flux_array, noise_array, periodic_wrap, l_offset)
            centers = centers - l_offset
            
        if not voigt:
            sigmas = np.array([fit.estimated_variables[i]['sigma'].value for i in range(n)])
        
        elif voigt:
            g_fwhms = np.array([fit.estimated_variables[i]['G_fwhm'].value for i in range(n)])
            sigmas = VPfit.GaussianWidth(g_fwhms)
       
        """
        flux_model['centers'] = np.append(flux_model['centers'], centers)
        flux_model['amplitude'] = np.append(flux_model['amplitude'], heights)
        flux_model['sigmas'] = np.append(flux_model['sigmas'], sigmas)
        print j,centers,heights,sigmas
        """

        # estimate uncertainties on fit parameters
        if mcmc_cov:
            cov = fit.chain_covariance(n, voigt=voigt)
            std_a = np.sqrt([cov[i][0][0] for i in range(n)])
            std_s = np.sqrt([cov[i][1][1] for i in range(n)])
            std_c = np.sqrt([cov[i][2][2] for i in range(n)])
            cov_as = np.array([cov[i][0][1] for i in range(n)])

            #flux_model['std_a'] = np.append(flux_model['std_a'], std_a)
            #flux_model['std_s'] = np.append(flux_model['std_s'], std_s)
            #flux_model['std_c'] = np.append(flux_model['std_c'], std_c)
            #flux_model['cov_as'] = np.append(flux_model['cov_as'], cov_as)

            line_params['dN'] = np.append(line_params['dN'], ErrorN(heights, sigmas, std_a, std_s, cov_as, sigma_0))
        
        elif get_mcmc_err:
            stats = fit.mcmc.stats()
            #TODO: figure out why this "std_s = " line is giving " KeyError: 'est_sigma_0' " when --voigt is used
            # (probably because it's called something else in the Voigt object?)
            """
             Traceback (most recent call last):
                 File "/home/jacobc/VAMP/vpfits.py", line 1082, in <module>
                     line_params, flux_model = fit_spectrum(wavelength, noise, flux, args.line, voigt=args.voigt, folder=args.output_folder)
                 File "/home/jacobc/VAMP/vpfits.py", line 905, in fit_spectrum
                     std_s = np.array([stats['est_sigma_'+str(i)]['standard deviation'] for i in range(n)])
             KeyError: 'est_sigma_0'
             """
            std_s = np.array([stats['est_sigma_'+str(i)]['standard deviation'] for i in range(n)]) #mcmc fitting error for "sigmas"
            std_a = np.array([stats['xexp_'+str(i)]['standard deviation'] for i in range(n)])      #mcmc fitting error for "amplitude" (referred to as "xexp"
            std_l = np.array([stats['est_centroid_'+str(i)]['standard deviation'] for i in range(n)])      #mcmc fitting error for "amplitude" (referred to as "xexp"

            #flux_model['std_s'] = np.append(flux_model['std_s'], std_s)
            #flux_model['std_a'] = np.append(flux_model['std_a'], std_a)

            line_params['db'] = np.append(line_params['db'], std_s*centers*np.sqrt(2))
            line_params['dl'] = np.append(line_params['dl'], std_l)
            covariance = [0] * len(std_a) # treat covariance as 0  TODO: figure out how to extract covariance (if possible) from MCMC chain.
            line_params['dN'] = np.append(line_params['dN'], ErrorN(heights, sigmas, std_a, std_s, covariance, sigma_0))

        line_params['b'] = np.append(line_params['b'], centers*1.e-13*sigmas*np.sqrt(2))   # 1.e-13 converts A to km, to get km/s
        line_params['N'] = np.append(line_params['N'], heights*sigmas*np.sqrt(2*np.pi)/sigma_0)  # sqrt(2pi) to undo normalization of Gaussian
        line_params['l'] = np.append(line_params['l'], centers)
        for k in range(n):
            line_params['region_number'] = np.append(line_params['region_number'], j)
            line_params['EW'] = np.append(line_params['EW'], EquivalentWidth(Tau2flux(fit.estimated_profiles[k].value), np.flip(wavelength_array[start:end], 0)))
       
    if outfile_spec:
        with h5py.File(outfile_spec, 'w') as f:
            for p in line_params.keys():
                f.create_dataset(p, data=np.array(line_params[p]))
            for p in flux_model.keys():
                f.create_dataset(p, data=np.array(flux_model[p]))
#        output_spectrum(line_params, outfile_spec, 'h5')
#        output_spectrum(flux_model, outfile_spec, 'h5')

    return line_params,flux_model

#####################################################
# Output + plot routines
#####################################################

def plot_spectrum(wavelength_array, flux_data, flux_model, regions, N=6):
    """
    Routine to plot the fits to the data. First plot is the total fit, second is the component
    Voigt profiles, third is the residuals.
    Args:
        wavelength_array (numpy array)
        flux_data (numpy array): data values for flux 
        flux_model (dict): dict output of the fitting routine, containing profiles for each 
                            component
        regions  (numpy array): pixel boundaries for the regions of the spectrum
        N (int): split the spectrum into N equal segments (for visibility), one panel per segment
    """

    def plot_bracket(x, axis, dir):
        height = .2
        arm_length = 0.05
        axis.plot((x, x), (1-height/2, 1+height/2), color='magenta')

        if dir=='left':
            xarm = x+arm_length
        if dir=='right':
            xarm = x-arm_length

        axis.plot((x, xarm), (1-height/2, 1-height/2), color='magenta')
        axis.plot((x, xarm), (1+height/2, 1+height/2), color='magenta')

    model = flux_model['flux']
    length = len(flux_data) / N
    fig, ax = plt.subplots(N, figsize=(15,15))
    for n in range(N):
        lower_lim = n*length
        upper_lim = n*length+length
        ax[n].set_xlim(wavelength_array[lower_lim], wavelength_array[upper_lim])        
        #print wavelength_array,flux_data,model

        ax[n].plot(wavelength_array, flux_data, c='black', label='Data')
        ax[n].plot(wavelength_array, model, c='green', label='Model')
        ax[n].set_xlim(wavelength_array[lower_lim], wavelength_array[upper_lim])
        for (start, end) in regions:
            plot_bracket(wavelength_array[start], ax[n], 'left')
            plot_bracket(wavelength_array[end], ax[n], 'right')

        '''
        for i in range(len(regions)):
            start, end = regions[i]
            region_data = flux_model['region_'+str(i)+'_flux']
            for j in range(len(region_data)):
                ax[n].plot(wavelength_array[start:end], region_data[j], '--', c='blue')
                minpix = np.argmin(region_data[j])+start
                ax[n].axvline(x=wavelength_array[minpix],ymin=0.93,ymax=0.97,color='blue')
        '''
    plt.xlabel('Wavelength (A)')
    plt.ylabel('Flux')
    plt.show()    

    '''
    fig, ax = plt.subplots(N, figsize=(15,15))
    for n in range(N):
        lower_lim = n*length
        upper_lim = n*length+length

        ax[n].plot(wavelength_array, (flux_data - model), c='blue')
        ax[n].hlines(1, wavelength_array[0], wavelength_array[-1], color='red', linestyles='-')
        ax[n].hlines(-1, wavelength_array[0], wavelength_array[-1], color='red', linestyles='-')
        ax[n].hlines(3, wavelength_array[0], wavelength_array[-1], color='red', linestyles='--')
        ax[n].hlines(-3, wavelength_array[0], wavelength_array[-1], color='red', linestyles='--')
        ax[n].set_xlim(wavelength_array[lower_lim], wavelength_array[upper_lim])
        for (start, end) in regions:
            plot_bracket(wavelength_array[start], ax[n], 'left')
            plot_bracket(wavelength_array[end], ax[n], 'right')

    plt.xlabel('Wavelength (A)')
    plt.ylabel('Flux')
    plt.savefig(folder+'residuals.png')
    plt.clf()
    '''

    return

def output_spectrum(params, filename, format):
    """
    Save file with physical parameters from fit
    Args:
        params (dict): output of fit_spectrum
        filename (string): name of ascii file
        format (string): file format to save. Can be ascii or h5.
    """
    if format == 'ascii':
        import astropy.io.ascii as ascii
        if 'N' in params:
            formats = {'N': '%.6g', 'dN': '%.6g', 'EW': '%.6g', 'b': '%.6g', 'db': '%.6g', 'centers': '%.6g',
                       'region_numbers': '%.6g'}
            ascii.write(params, filename, formats=formats)
        elif 'tau' in params:
            formats = {'tau': '%.6g', 'chi_squared': '%.6g', 'centers': '%.6g', 'amplitude': '%.6g', 'sigmas': '%.6g', 
                        'std_a': '%.6g', 'std_s': '%.6g', 'std_c': '%.6g', 'cov_as': '%.6g'}
            ascii.write(params, filename, formats=formats)
    if format == 'h5':
        with h5py.File(filename, 'a') as f:
            for p in params.keys():
                f.create_dataset(p, data=np.array(params[p]))

